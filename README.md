Webfaction Bare Startup Project
===============================

create a virtualenv in project dir
----------------------------------

    virtualenv --no-site-packages myenv

Webfaction settings
-------------------

`you need to edit places where marked with **[edit]** in **6** places(2 in wsgi.py & 4 in http.conf)`

**wsgi.py**

    import os,sys

    os.environ['DJANGO_SETTINGS_MODULE'] = '[edit].settings'

    virtenv =os.path.join(os.environ['HOME'],'webapps','[edit]','myenv','bin','activate_this.py')
    try:
        execfile(virtenv, dict(__file__=virtenv))
    except IOError:
        pass

    from django.core.handlers.wsgi import WSGIHandler


**apache2/conf/httpd.conf**

    ......skipped 21 lines.....
    WSGIPythonPath /home/suhailvs/webapps/facebookauth:/home/suhailvs/webapps/facebookauth/[edit]:/home/suhailvs/webapps/facebookauth/lib/python2.7
    WSGIDaemonProcess facebookauth processes=2 threads=12 python-path=/home/suhailvs/webapps/facebookauth:/home/suhailvs/webapps/facebookauth/[edit]:/home/suhailvs/webapps/facebookauth/lib/python2.7
    WSGIProcessGroup facebookauth
    WSGIRestrictEmbedded On
    WSGILazyInitialization On
    WSGIScriptAlias / /home/suhailvs/webapps/facebookauth/[edit]/[edit]/wsgi.py