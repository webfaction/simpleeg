from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'sampleauth.views.home', name='home'),
    url(r'^logout/$', 'django.contrib.auth.views.logout'),
    
    url(r'^cas/', include('mama_cas.urls')),
    # url(r'^blog/', include('blog.urls')),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^admin/', include(admin.site.urls)),
)
