from django.shortcuts import render
from social.backends.utils import load_backends
from django.conf import settings
from django.contrib.auth.models import User
# Create your views here.
def home(request):
    backends= load_backends(settings.AUTHENTICATION_BACKENDS)
    backends = [(name, backend) for name, backend in backends.items()
                    if name not in ['username', 'email']]
    backends.sort(key=lambda b: b[0])
    avail_backends= [backends[n:n + 10] for n in range(0, len(backends), 10)]
    return render(request,'home.html',{'backends':avail_backends,'user_list':User.objects.all()})
